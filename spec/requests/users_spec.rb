require 'rails_helper'

RSpec.describe "Users", type: :request do
  describe "GET /index" do
    it 'returns all the users' do 
      user1 = User.create(name:"test1", email:"test1@gmail.com", password_digest:"1234567")
      user2 = User.create(name:"BBB", email:"BBB@gmail.com", password_digest:"1234567")
      get '/users'
      
      #expect(JSON.parse(response.body).size).to eq(2)
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET /show' do 
    it 'show user' do 
      user1 = User.create(name: "test1", email:"test1@gmail.com", password: "123456", password_confirmation:"123456")
      get '/users/1'
      expect(JSON.parse(response.body)['name']).to eq(user1.name)
    end 
  end 

  describe 'POST /users' do 
    it 'create a user' do 
      post '/users', params:{user:{name:"111", email:"111@gmail.com", password:"123456", password_confirmation:"123456"}}
      
      expect(JSON.parse(response.body)['name']).to eq('111')
    end
  end

  describe 'PUT /users/:id' do 
    user1 = User.create(name:"test1", email:"test1@gmail.com", password_digest:"1234567")

    it 'update user' do 
      put '/users/1', params: {user:{name:'AAA'}}
      
      expect(JSON.parse(response.body)['name']).to eq('AAA')
    end 
  end 

  describe 'DELETE /users/:id' do 
    user1 = User.create(name:"AAA", email: "AAA@gmail.com", password: "123456", password_confirmation:"123456")
    it 'should destroy user' do 
      delete '/users/1'
      expect(response).to have_http_status(204)
    end 
  end 
end
