require 'rails_helper'

RSpec.describe "Tokens", type: :request do
  describe "GET /index" do
    #pending "add some examples (or delete) #{__FILE__}"
  end

  describe "POST /tokens" do 
          user = User.create(name:"BBB", email:"BBB@gmail.com", password:"123456")

    it 'should generate token when login' do 
     post '/tokens', params: { email:user.email, password:user.password }
            byebug  
     expect(response).to have_http_status(:success)
     expect(JSON.parse(response.body)["token"]).not_to be_nil
     expect(JSON.parse(response.body)['email']).to eq(user.email)

   end 
 end 

end
